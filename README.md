Pre Account Creation Invite Test
================================

Test #1
-------
1. Manually invited an email address to join this GitLab project
2. Created Developer account using same email address
3. Logged into gitlab.wikimedia.org with new Developer account via IDP
4. Visited "Join now" link from email sent to new Developer account email

* Manually inviting an email address via the GitLab web ui triggers an email to the provided address.
* Attaching a Developer account via login with a previously invited email address **does not** immediately result in project membership.
* Visiting the "Join now" link from the GitLab sent email message **does** immediately result in project membership when already authenticated.

Test #2
-------
1. Created Developer account
2. Manually invited Developer account's email address to join this GitLab project
3. Visited "Join now" link from email sent to new Developer account email
4. Redirected to IDP login
5. Authenticated to IDP
6. Redirected to this project's activity stream
7. Visited "Join now" link from email sent to new Developer account email again

* IDP flow seems to interrupt invite acceptance. Account was not connected as a member of this repo until visiting the invite link for a second time after authenticating.

Test #3
-------
1. Created Developer account
2. Invited Developer account's email address to join this GitLab project via the [`POST /api/v4/projects/:id/invitations` API endpoint](https://docs.gitlab.com/ee/api/invitations.html#add-a-member-to-a-group-or-project)
3. Visited "Join now" link from email sent to new Developer account email
4. Redirected to IDP login
5. Authenticated to IDP
6. Redirected to this project's activity stream
7. Visited "Join now" link from email sent to new Developer account email again

* API invitation works the same as the manual invitation
